<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.edu.eastbay.fb.*, com.restfb.types.User, java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Tweet Page</title>
<link rel="stylesheet" type="text/css" href="homepage.css"/>
<script type="text/javascript" src="/js/scriptfile.js"></script>
</head>
<body>
	<jsp:include page="NavBar.jsp"/>
	<%
	User user = request.getSession().getAttribute("me_object") != null? (User)request.getSession().getAttribute("me_object") : null;
	boolean isSendDirect = request.getSession().getAttribute(ConstantNames.IS_SEND_DIRECT) != null ? (boolean)request.getSession().getAttribute(ConstantNames.IS_SEND_DIRECT) : false;
	String message = request.getSession().getAttribute(ConstantNames.TWEET_CONTENT) != null ? (String)request.getSession().getAttribute(ConstantNames.TWEET_CONTENT) : "";
	%>
	
	<% if(isSendDirect && message !=null) {%>
		Hi there go ahead its working
		<script type="text/javascript">message="<%= message%>"</script>
		<script type="text/javascript">userID="<%= user.getId()%>"</script>
		<script type="text/javascript">sendTweetDirectly(message);</script>
	<% session.setAttribute(ConstantNames.IS_SEND_DIRECT, false);
	session.setAttribute(ConstantNames.TWEET_CONTENT, null);
	}%>
	
	<h3>Tweet a Friend </h3>
	<form action="/shareTweet" method="post">	
		<table>
			<tr>
				<td rowspan="2">
				<textarea id="text_content" name="text_content"
						class="textarea" rows="5">
				</textarea>
				</td>
				<td>
								Tweet Options: 
				<select name="TweetOptions">
						<option value="PostOnTimeline" > Post on Timeline </option>
						<option value="SendDirectMessage" > Send Direct Message </option>
				</select> 
				</td>
			</tr>

			<tr>
				<td>
				<input type="submit" value="submit" />
				</td>
			</tr>

		</table>
	</form>
	<form action="/viewTweets" method="post">
		<br/><br/>
		<input type="submit" value="View Tweets" />
	</form>	
	
</body>
</html>
