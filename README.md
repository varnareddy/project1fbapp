AppDisplayName: SasiVarnaProject1TweetApp

 Intro and Purpose:

Main purpose of this Project is to create a Facebook web app which uses google data store for deployment and entity storage of App User data and their tweets.
I have implemented 3 main requirements of this app as below.
1)	Ability to Tweet on time line
2)	Able to view Friends details along with their posted tweet content
3)	Top 10 tweets of app based on tweet views count
This app will use google data store for storing and retrieving user data and their respective tweets.
I have a tweet view counter which will get incremented when users friends tab is seen or when view my tweets is clicked.
Also my app has the facility for a logged in user to delete their tweet which will automatically deletes data from 
my google data store entity as well. I have an entity named UserTweets in my data store for all user and tweet 
data with a unique key.

FBAppURL: https://apps.facebook.com/1982588915345394

Code Description:
ConstantNames.java     I have put all constant names  values for the entire project use
Constants.java    I have put all constant values for the entire project use
DeleteUserTweet.java --> logic for deleting user based on id
FacebookSignedRequest.java --> for facebook authentication check
FriendDetails.java -> logic for retrieving user’s friends
GetFriendsAndTweets.java -> for displaying friends and their tweet details on app
GetPictureURL.java -> For retrieval of Display pic of facebook of a user
ShareTweet.java -> to post tweet on time line
TweetAppFbServlet.java -> checking the access of user for my app
TweetData.java ->  tweet object creation
ViewFriends.java -> logic of finding  friends of loggined user 
ViewTweets.java -> java logic for retrieving tweets of app users
GAEDataStore.java -> for storing entered and tweet and respective user
TweetKind.java -> entity for tweet data in data store
FirstPage.jsp -> home page Screen
Friends.jsp -> User Friends page  display
Homepage.css -> Styling for myconstant  Navigation bar through out diaplay
Index.html -> not really using 
Index.jsp -> for welcome file and to redirect to servlet 
Scriptfile.js -> for direct message
NavBar.jsp -> default navigation bar code
TopTenTweets.jsp -> Display page for Top 10 Tweets page
Tweet.jsp -> Tweet page view
ViewMyFriends.jsp -> friends  page display
viewMyTweets.jsp -> viewmytweets button display page
web.xml -> Servlet and welcome file configurations.

