package com.edu.eastbay.gau;

public class TweetKind {
	String userId;
	String username;
	String textContent;
	String tweetOption;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getTextContent() {
		return textContent;
	}
	public void setTextContent(String textContent) {
		this.textContent = textContent;
	}
	public String getTweetOption() {
		return tweetOption;
	}
	public void setTweetOption(String tweetOption) {
		this.tweetOption = tweetOption;
	}
	
}
