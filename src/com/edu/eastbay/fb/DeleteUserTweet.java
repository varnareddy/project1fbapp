package com.edu.eastbay.fb;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.edu.eastbay.gau.GAEDataStore;
import com.restfb.types.User;

public class DeleteUserTweet extends HttpServlet {
	public void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute(ConstantNames.LOGIN_USER);
		GAEDataStore gaeDataStoreInstance = GAEDataStore.getInstance();
		
		if(request.getParameter("Delete") == null) {
			response.getWriter().println("Some issue with delete");
			
			/*Enumeration<String> paramNames = request.getParameterNames();
			String params = "";
			while(paramNames.hasMoreElements())
				params = params + " -- ";
			response.getWriter().println(params);*/
			return;
		}
		
		gaeDataStoreInstance.removeUserTweet(Long.valueOf(request.getParameter("Delete")));
		
		request.getRequestDispatcher("/viewTweets").forward(request, response);
		return;
	}
}
