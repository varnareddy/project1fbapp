package com.edu.eastbay.fb;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.edu.eastbay.gau.GAEDataStore;
import com.google.appengine.api.datastore.Entity;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.types.FacebookType;
import com.restfb.types.User;

public class ShareTweet extends HttpServlet {
	public void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute(ConstantNames.LOGIN_USER);
		String auth_token = (String) session.getAttribute(ConstantNames.O_AUTH_TOKEN);
		GAEDataStore gaeDataStoreInstance = GAEDataStore.getInstance();
		
		boolean isSendDirect = false;
		
		String userTweet = request.getParameter(ConstantNames.TWEET_CONTENT);
		String optionChose = request.getParameter(ConstantNames.TWEET_OPTION);
		
		Entity afterSave = gaeDataStoreInstance.saveUserTweet(user.getId(), user.getName(), userTweet, optionChose);
		//response.getWriter().println(afterSave.getProperty(ConstantNames.TWEET_CONTENT));
		//return;
		
		FacebookClient facebookClient = new DefaultFacebookClient(auth_token);
		FacebookType publishMessageResponse = null;
		
		if(ConstantNames.POST_ON_TIME_LINE.equals(optionChose)) {
			publishMessageResponse = facebookClient.publish("me/feed", FacebookType.class, Parameter.with("message", afterSave.getProperty(ConstantNames.TWEET_CONTENT)));
			response.sendRedirect("tweet.jsp");
			return;
		} else if(ConstantNames.SEND_DIRECT_MESSAGE.equals(optionChose)){
			//publishMessageResponse = facebookClient.publish("send", FacebookType.class, Parameter.with("message", afterSave.getProperty(ConstantNames.TWEET_CONTENT)));
			//response.getWriter().println("<script></script>");
			session.setAttribute(ConstantNames.IS_SEND_DIRECT, true);
			session.setAttribute(ConstantNames.TWEET_CONTENT, userTweet);
			
			response.sendRedirect("tweet.jsp");
			return;
		}
		
	}
}
