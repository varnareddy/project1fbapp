package com.edu.eastbay.fb;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

import com.restfb.Connection;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.types.User;

/**
 * This class is referenced from document : https://www.ibm.com/developerworks/library/j-fb-gae/index.html#artrelatedtopics
 * @author sasivarna
 *
 */
public class TweetAppFbServlet extends HttpServlet {

	private FacebookClient facebookClient;

	public void doPost(HttpServletRequest request, HttpServletResponse response) 
		throws ServletException, IOException {
		
		String sinReq = (String) request.getParameter("signed_request");
		
		
		FacebookSignedRequest fbSR = null;
		try {
			fbSR = FacebookSignedRequest.getFacebookSignedRequest(sinReq);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String oauthToken = fbSR.getOauth_token();
				
		PrintWriter writer = response.getWriter();
		if(oauthToken == null) {
			
			response.setContentType("text/html");
			String authURL = "https://www.facebook.com/dialog/oauth?client_id="
								+ Constants.API_KEY + "&redirect_uri=https://apps.facebook.com/1982588915345394/&scope=";
			writer.print("<script> top.location.href='"	+ authURL + "'</script>");
			writer.close();

		}
		else {
			
			facebookClient = new DefaultFacebookClient(oauthToken);
			User usr = facebookClient.fetchObject("me", User.class);
			

			HttpSession session = request.getSession();
			if(usr!= null) {
				session.setAttribute("me_object", usr);
			}
			
			if(oauthToken != null) {
				session.setAttribute(ConstantNames.O_AUTH_TOKEN, oauthToken);
			}
			
			RequestDispatcher rDispatcher = request.getRequestDispatcher("FirstPage.jsp");
			rDispatcher.forward(request, response);
		}
		
	}

}
