package com.edu.eastbay.fb;

public interface ConstantNames {
	
	String LOGIN_USER = "me_object";
	String O_AUTH_TOKEN = "oauthToken";
	String LOGIN_USER_ID = "id";
	String LOGIN_USER_NAME = "loginUserName";
	String COUNT = "count";
	
	String TWEET_CONTENT = "text_content";
	String TWEET_OPTION = "TweetOptions";
	String POST_ON_TIME_LINE = "PostOnTimeline";
	String SEND_DIRECT_MESSAGE = "SendDirectMessage";
	String IS_SEND_DIRECT = "isSendDirect";
	
	String DATE = "date";
	String DELETE_TWEET_BUTTON = "deleteTbutton";
	
	//Tables / Kinds
	String USER_TWEETS="UserTweets";
	
	//Query Result
	String USER_TWEETS_MAP = "UserTweetsMap";
}
