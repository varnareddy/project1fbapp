package com.edu.eastbay.fb;

public class FriendDetails {
	String name;
	String imageSource;
	Long tweetCount;
	String userID;
	//String tweets;
	
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImageSource() {
		return imageSource;
	}
	public void setImageSource(String imageSource) {
		this.imageSource = imageSource;
	}
	public Long getTweetCount() {
		return tweetCount;
	}
	public void setTweetCount(Long tweetCount) {
		this.tweetCount = tweetCount;
	}
	
	
}
